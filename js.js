// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад доданий в репозиторії групи, посилання на ДЗ вище), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
// Після отримання підтвердження із сервера(запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

let URL_USERS = "https://ajax.test-danit.com/api/json/users";
let URL_POSTS = "https://ajax.test-danit.com/api/json/posts";
const URL_DELETE = "https://ajax.test-danit.com/api/json/posts/";
const root = document.querySelector("#root");

const template = document.getElementById("posts-list");
const fragment = template.content.cloneNode(true);

class Card {
  #list = document.createElement("ul");

  request(url) {
    return fetch(url)
      .then((response) => {
        return response.json();
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }
  delete(post) {
    return fetch(`${URL_POSTS}/${post.id}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return true;
      })
      .catch((error) => {
        console.error("There was a problem with the DELETE request:", error);
        return false;
      });
  }

  get(url) {
    return this.request(url);
  }
  render(userData, postData) {
    userData.forEach((user) => {
      const userId = user.id;
      const userPosts = postData.filter((post) => post.userId === userId);
      const userName = user.name;
      const userEmail = user.email;

      userPosts.forEach((post) => {
        let postusId = post.id;

        const listItem = document.createElement("li");
        listItem.classList.add("card-list");
        listItem.setAttribute("id", `${postusId}`);

        const fragment = template.content.cloneNode(true);

        const postTitle = post.title;
        let title = document.createElement("p");
        title.classList.add("twitter__text-user-info");
        title.setAttribute("id", `twitter-title`);

        title.textContent = postTitle;

        const postBody = post.body;
        let body = document.createElement("p");
        body.classList.add("twitter__text-user-info");
        body.setAttribute("id", `twitter-body`);

        body.textContent = postBody;

        let photo = fragment.querySelector(".twitter__photo");
        photo.src = "user.png";

        let namus = fragment.querySelector("#twitter-name");
        namus.textContent = userName;

        let email = fragment.querySelector("#twitter-email");
        email.textContent = userEmail;

        const twitter = fragment.querySelector(".twitter");
        const divUser = fragment.querySelector(".twitter__text-user");
        divUser.append(namus, email);

        const divTwitter = fragment.querySelector(".twitter__text");

        const delButton = fragment.querySelector("#delBtn");

        delButton.addEventListener("click", () => {
          console.log(post);
          card
            .delete(post)
            .then((data) => {
              console.log(data);
              listItem.remove();
            })
            .catch((error) => {
              console.log(error);
            });
        });

        divTwitter.append(divUser, title, body);
        twitter.append(photo, divTwitter, delButton);
        listItem.append(twitter);
        this.#list.append(listItem);
      });
    });

    return this.#list;
  }
}

const card = new Card();

card.get(URL_USERS).then((userData) => {
  card.get(URL_POSTS).then((postData) => {
    root.append(card.render(userData, postData));
  });
});

//колишній метод реалізації
// const requestOptions = {
//   method: "DELETE",
//   headers: {
//     "Content-Type": "application/json",
//   },
// };
// fetch(`${URL_DELETE}${postusId}`, requestOptions)
//   .then((response) => {
//     if (!response.ok) {
//       throw new Error("Відповідь мережі була неправильною");
//     }
//     console.log(`${URL_DELETE}${postusId}`);
//     alert("Пост успішно видалено!");
//     listItem.remove();
//   })
//   .catch((error) => {
//     console.error("Виникла проблема з операцією отримання:", error);
//   });
